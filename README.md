# Client-side (frontend) specification

The client-side application was written in use with the following third-party packages, libraries and frameworks:

- [Svelte](https://github.com/sveltejs/svelte "Svelte Github repository")
- [rollup](https://github.com/rollup/rollup "Rollup Github repository")
- [flexboxgrid](https://github.com/kristoferjoseph/flexboxgrid "Flexboxgrid Github repository")


## Deployment using NPM

To deploy this project using [node package manager](https://www.npmjs.com/ "NPM official website") do the following:

1. Install dependencies by executing `npm install`
2. Build the project soruces using `npm run build`
3. Start the SIRV server: `npm run start`
